import { Component } from "react";

class Info extends Component {
    render() {
        const { firstName, lastName, favNumber } = this.props;

        return (
            <div>My name is {lastName} {firstName} and my favourite number is {favNumber}. </div>
        )
    }
}

export default Info;